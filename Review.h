class Review {

private:
	string name;
	string label;
	string comment;
	bool checked;

public:
	bool getChecked();

	void setChecked(bool checked);
};
